HYPEZIG is an Android app to browse local cultural events in Leipzig, Germany.

## Downloads

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.kolloware.hypezigapp/)

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=com.kolloware.hypezigapp)

## Technical details

The app scrapes the events listed on the public event websites, such as https://kreuzer-leipzig.de.
All events within a time range of one month are downloaded.

Scraping is only performed when the user explicitly downloads new contents or activates automatic downloads.


## License

This software is released under the terms of the [GNU General Public License](http://www.gnu.org/licenses/gpl-3.0.html).
