package com.kolloware.hypezigapp.ui;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.models.Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;
import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>
        implements Filterable {

    private Context context;

    SimpleDateFormat month = new SimpleDateFormat("MMM");
    SimpleDateFormat day = new SimpleDateFormat("dd");
    SimpleDateFormat hours = new SimpleDateFormat("EEE, HH:mm");

    List<Event> eventsToDisplay;
    List<Event> dataSource;

    RecyclerView recyclerView;
    TextView emptyMessage, noDownloadersMessage;

    public RecyclerViewAdapter(Context context, List<Event> dataSource,
                               RecyclerView recyclerView, TextView emptyMessage,
                               TextView noDownloadersMessage) {
        Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

        this.context = context;
        this.dataSource = dataSource;
        this.eventsToDisplay = new ArrayList<>(dataSource);

        this.recyclerView = recyclerView;
        this.emptyMessage = emptyMessage;
        this.noDownloadersMessage = noDownloadersMessage;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateViewHolder() called with: parent = ["
                + parent + "], viewType = [" + viewType + "]");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem,
                parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onBindViewHolder() called with: holder = ["
                + holder + "], position = [" + position + "]");
        final Event e = eventsToDisplay.get(position);

        holder.month.setText(month.format(e.date)
                .replace(".", "")
                .substring(0, 3)
                .toUpperCase());
        holder.title.setText(e.title);
        holder.date.setText(day.format(e.date));
        holder.time.setText(hours.format(e.date));
        holder.category.setText("| " + context.getString(UITools.getCategoryResourceId(e)));
        holder.location.setText(e.locationName.toUpperCase());
        holder.favorite.setImageResource(getFavoriteImageResource(e.favorite));

        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_UI, getClass().getSimpleName() + ".onClick() called with: v = ["
                        + v + "]");
                e.favorite = !e.favorite;
                ((ImageButton) v).setImageResource(getFavoriteImageResource(e.favorite));

                (new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... voids) {
                        Log.d(LOG_UI, "RecyclerViewAdapter / anonymous inner class "
                                + ".doInBackground() called");
                        AppDatabase.getInstance(context).eventDao().update(e);
                        return null;
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(LOG_UI, getClass().getSimpleName() + ".onClick() called with: v = ["
                        + v + "]");
                Intent intent = new Intent(context, EventDetailsActivity.class);
                Bundle b = new Bundle();
                b.putSerializable("eventData", e);
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });
    }

    private int getFavoriteImageResource(boolean isSelected) {
        return isSelected ? R.drawable.icon_star_on : R.drawable.icon_star_off;
    }

    @Override
    public int getItemCount() {
        return eventsToDisplay.size();
    }

    @Override
    public Filter getFilter() {
        return searchFilter;
    }

    private Filter searchFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            Log.d(LOG_UI, getClass().getSimpleName() + ".performFiltering() called with: "
                    + "constraint = [" + constraint + "]");
            List<Event> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataSource);
            }
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                Set<Category> categoriesBySearchTerm = UITools.getInstance(context)
                        .getCategoriesBySearchTerms(filterPattern);

                for (Event forEvent : dataSource) {
                    if (matchesSearchTerm(forEvent.title, filterPattern)
                            || matchesSearchTerm(forEvent.locationName, filterPattern)
                            || matchesSearchTerm(forEvent.subtitle, filterPattern)
                            || matchesSearchTerm(forEvent.details, filterPattern)
                            || categoriesBySearchTerm.contains(forEvent.category)) {
                        filteredList.add(forEvent);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        private boolean matchesSearchTerm(String inString, String inFilterPattern) {
            if (inString != null && inString.toLowerCase().contains(inFilterPattern)) {
                return true;
            }

            return false;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            Log.d(LOG_UI, getClass().getSimpleName() + ".publishResults() called with: "
                    + "constraint = [" + constraint + "], results = [" + results + "]");
            eventsToDisplay.clear();
            eventsToDisplay.addAll((List) results.values);
            refreshView();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView month, date, time, title, category, location;
        RelativeLayout parentLayout;
        ImageButton favorite;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

            month = itemView.findViewById(R.id.month);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            title = itemView.findViewById(R.id.title);
            category = itemView.findViewById(R.id.category);
            location = itemView.findViewById(R.id.location);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            favorite = itemView.findViewById(R.id.favorite_button);
        }
    }

    public void updateEventsToDisplay(List<Event> newList) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".updateEventsToDisplay() called with: "
                + "newList = [" + newList + "]");

        List<Event> newResults;

        if (newList != null) {
            newResults = new ArrayList<>(newList);
        }
        else {
            newResults = dataSource;
        }

        this.eventsToDisplay.clear();
        this.eventsToDisplay.addAll(newResults);

        refreshView();
    }

    private void refreshView() {
        notifyDataSetChanged();

        if (eventsToDisplay.size() == 0) {

            if ((noDownloadersMessage != null)
                && (Model.getInstance().getDownloadersToShow().isEmpty())) {
                emptyMessage.setVisibility(View.GONE);
                noDownloadersMessage.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
            else {
                emptyMessage.setVisibility(View.VISIBLE);
                if (noDownloadersMessage != null) noDownloadersMessage.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
            }
       }
        else {
            emptyMessage.setVisibility(View.GONE);
            if (noDownloadersMessage != null) noDownloadersMessage.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
