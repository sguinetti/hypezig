package com.kolloware.hypezigapp.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.kolloware.hypezigapp.R;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class AboutActivity extends AppCompatActivity {

    public static final String URL_FAQ = "https://gitlab.com/intergalacticsuperhero/hypezig/" +
            "-/wikis/H%C3%A4ufig-gestellte-Fragen";
    public static final String URL_SOURCE_CODE = "https://gitlab.com/intergalacticsuperhero/hypezig";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_about);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle(R.string.drawer_nav_about);

        TextView faq = findViewById(R.id.about_faq);
        faq.setText(Html.fromHtml("<a href='" + URL_FAQ + "'>" +
                getString(R.string.about_faq) + "</a>"));
        faq.setMovementMethod(LinkMovementMethod.getInstance());

        TextView gitLab = findViewById(R.id.about_gitlab);
        gitLab.setText(Html.fromHtml("<a href='" + URL_SOURCE_CODE + "'>" +
                getString(R.string.about_gitlab) + "</a>"));
        gitLab.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        this.finish();
        return true;
    }
}
