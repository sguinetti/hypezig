package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.textfield.TextInputLayout;
import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.tasks.EventScraperTask;
import com.kolloware.hypezigapp.tasks.UpdateDownloaderTask;
import com.kolloware.hypezigapp.tasks.VoidCallback;

public class StartScrapingDialog extends AppCompatDialogFragment {

    private DownloadsRecyclerViewAdapter adapter;

    private Downloader downloader;

    public StartScrapingDialog(Downloader downloader, DownloadsRecyclerViewAdapter adapter) {
        this.downloader = downloader;
        this.adapter = adapter;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_auto_download);
        builder.setMessage(R.string.dialog_auto_download_question);
        builder.setNegativeButton(R.string.dialog_auto_download_no, null);
        builder.setPositiveButton(getString(R.string.dialog_auto_download_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        (new EventScraperTask(downloader, adapter, getContext(),
                                new VoidCallback(), true))
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                });

        Dialog localResult = builder.create();

        return localResult;
    }

}
