package com.kolloware.hypezigapp.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.db.DownloaderDao;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.tasks.DeleteDownloaderTask;
import com.kolloware.hypezigapp.tasks.EventScraperTask;
import com.kolloware.hypezigapp.tasks.VoidCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;
import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;
import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class DownloadsRecyclerViewAdapter extends RecyclerView.Adapter<DownloadsRecyclerViewAdapter.ViewHolder>
    implements UICallback {

    private Context context;
    private RecyclerView recyclerView;
    private TextView emptyMessage;


    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy - HH:mm");


    public DownloadsRecyclerViewAdapter(final Context context, RecyclerView recyclerView, TextView emptyMessage) {
        Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

        this.context = context;
        this.recyclerView = recyclerView;
        this.emptyMessage = emptyMessage;

        (new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                Log.d(LOG_UI, "DownloadsRecyclerViewAdapter / anonymous inner class "
                        + ".doInBackground() called");

                List<Downloader> downloaders = AppDatabase.getInstance(context).downloaderDao().getAll();

                List<Downloader> downloadersToShow = Model.getInstance().getDownloadersToShow();

                downloadersToShow.clear();
                downloadersToShow.addAll(downloaders);

                Log.v(LOG_UI, "Downloaders: " + downloadersToShow);

                refresh();

                return null;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateViewHolder() called with: parent = ["
                + parent + "], viewType = [" + viewType + "]");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_downloaditem,
                parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onBindViewHolder() called with: holder = ["
                + holder + "], position = [" + position + "]");

        List<Downloader> downloadersToShow = Model.getInstance().getDownloadersToShow();
        final Downloader d = downloadersToShow.get(position);

        holder.title.setText(d.getTitle());
        holder.progressBar.setIndeterminate(false);
        holder.progressBar.setProgress(d.getProgress());
        holder.updatedAt.setText(context.getString(R.string.task_last_update) + " "
                + ((d.getLastUpdate() != null) ? dateFormat.format(d.getLastUpdate()) : "-"));

        String resultText = "";
        if (d.getResult() != null) {
            switch(d.getResult()) {
                case INITIALIZING:
                    resultText = context.getString(R.string.task_status_init);
                    holder.progressBar.setIndeterminate(true);
                    break;
                case RUNNING:
                    resultText = context.getString(R.string.task_status_running);
                    break;
                case FINALIZING:
                    resultText = context.getString(R.string.task_status_finalizing);
                    holder.progressBar.setIndeterminate(true);
                    break;
                case SUCCESS:
                    resultText = context.getString(R.string.task_status_success);
                    break;
                case CANCELLED:
                    resultText = context.getString(R.string.task_status_cancelled);
                    break;
                case ERROR:
                    resultText = context.getString(R.string.task_status_error);
                    break;
            }
        }

        holder.scrapingResult.setText(context.getString(R.string.task_result) + ": "
                + resultText);

        if (d.isRunning()) {
            holder.downloadButton.setImageResource(R.drawable.icon_cancel);

            holder.downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.cancelTask();
                }
            });

            holder.updatedAt.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.VISIBLE);
        }
        else {
            holder.downloadButton.setImageResource(R.drawable.icon_download);
            holder.downloadButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    d.setProgress(0);
                    d.setRunning(true);
                    d.setLastUpdate(null);
                    d.setResult(DownloadStatus.RUNNING);

                    notifyDataSetChanged();

                    EventScraperTask task = new EventScraperTask(d,
                            DownloadsRecyclerViewAdapter.this,
                            context,
                            new VoidCallback(),
                            true);

                    task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            });

            holder.updatedAt.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.GONE);
        }


        holder.editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditDownloaderDialog dialog = new EditDownloaderDialog(d, DownloadsRecyclerViewAdapter.this);
                dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "add_downloader_dialog");
            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle(R.string.downloader_delete_title);
                builder.setMessage(d.getTitle());
                builder.setNegativeButton(R.string.dialog_button_cancel, null);
                builder.setPositiveButton(R.string.dialog_button_okay, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(LOG_DATA, getClass().getSimpleName()
                                + ".onClick() called with: dialog = ["
                                + dialog + "], which = [" + which + "]");
                        Log.i(LOG_DATA, "Delete downloader: " + d);

                        // remove from database
                        (new DeleteDownloaderTask(d, context))
                                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        // remove from display
                        Model.getInstance().getDownloadersToShow().remove(d);
                        refresh();
                    }
                });

                builder.create().show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Model.getInstance().getDownloadersToShow().size();
    }

    @Override
    public void refresh() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".refresh() called");

        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();

                if (Model.getInstance().getDownloadersToShow().size() == 0) {
                    emptyMessage.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
                else {
                    emptyMessage.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, updatedAt, scrapingResult;
        ProgressBar progressBar;
        ImageButton editButton, deleteButton, downloadButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

            title = itemView.findViewById(R.id.title);
            progressBar = itemView.findViewById(R.id.progress_bar);
            downloadButton = itemView.findViewById(R.id.download_button);
            editButton = itemView.findViewById(R.id.edit_button);
            deleteButton = itemView.findViewById(R.id.delete_button);
            updatedAt = itemView.findViewById(R.id.updated_at);
            scrapingResult = itemView.findViewById(R.id.scraping_result);
        }
    }
}
