package com.kolloware.hypezigapp.ui;

import android.webkit.URLUtil;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.textfield.TextInputLayout;
import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.ScraperTypeInformation;

import java.util.List;

abstract public class AbstractDownloaderDialog extends AppCompatDialogFragment {

    protected DownloadsRecyclerViewAdapter adapter;

    protected ScraperTypeInformation scraperTypeInformation;

    public AbstractDownloaderDialog(ScraperTypeInformation inScraperType, DownloadsRecyclerViewAdapter adapter) {
        this.scraperTypeInformation = inScraperType;
        this.adapter = adapter;
    }

    protected boolean validateField(TextInputLayout inField) {
        String title = inField.getEditText().getText().toString().trim();

        if (title.isEmpty()) {
            inField.setError(getString(R.string.edit_downloader_error_empty));
            return false;
        }
        else {
            inField.setError(null);
            return true;
        }
    }

    protected boolean validateFieldForURL(TextInputLayout inField) {
        String text = inField.getEditText().getText().toString().trim();

        if (!URLUtil.isValidUrl(text)) {
            inField.setError(getString(R.string.edit_downloader_error_url));
            return false;
        }
        else {
            inField.setError(null);
            return true;
        }
    }

    protected int getInsertionPosition(String stringToInsert, List<Downloader> list) {
        for (int i = 0; i < list.size(); i++) {
            String forString = list.get(i).getTitle();
            if (stringToInsert.compareTo(forString) <= 0) {
                return i;
            }
        }

        return list.size();
    }
}
