package com.kolloware.hypezigapp.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.tasks.MultiDownloader;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;


public class DownloadsFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateView() called with: inflater = ["
                + inflater + "], container = [" + container + "], savedInstanceState = ["
                + savedInstanceState + "]");

        return inflater.inflate(R.layout.fragment_downloads, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onViewCreated() called with: view = ["
                + view + "], savedInstanceState = [" + savedInstanceState + "]");

        super.onViewCreated(view, savedInstanceState);
        initRecyclerView(view);
    }


    private void initRecyclerView(@NonNull View view) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".initRecyclerView() called with: view = ["
                + view + "]");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewDownloads);

        DownloadsRecyclerViewAdapter adapter = Model.getInstance().getDownloadsRecyclerViewAdapter();

        if ((adapter == null) || Model.getInstance().getDownloadersToShow().isEmpty()) {
            adapter = new DownloadsRecyclerViewAdapter(getActivity(),
                    recyclerView,
                    (TextView) view.findViewById(R.id.emptyView));
            Model.getInstance().setDownloadsRecyclerViewAdapter(adapter);
        }

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.refresh();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateOptionsMenu() called with: menu = ["
                + menu + "], inflater = [" + inflater + "]");
        inflater.inflate(R.menu.downloader_menu, menu);
    }

    private void showAddDownloaderDialog() {
        ChooseScraperTypeDialog dialog = new ChooseScraperTypeDialog(
                Model.getInstance().getDownloadsRecyclerViewAdapter());
        dialog.show(getActivity().getSupportFragmentManager(), "choose_scraper_type_dialog");
    }

    private void showConfirmMultiDownloadDialog() {
        if (Model.getInstance().isMultiDownloadRunning()) {
            Toast.makeText(getContext(), R.string.dialog_multi_download_running, Toast.LENGTH_LONG).show();
        }
        else {
            MultiDownloadDialog dialog = new MultiDownloadDialog(
                    Model.getInstance().getDownloadsRecyclerViewAdapter());
            dialog.show(getActivity().getSupportFragmentManager(), "confirm_multi_download");
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onOptionsItemSelected() called with: item = ["
                + item + "]");

        switch (item.getItemId()) {
            case R.id.item_add_downloader:
                showAddDownloaderDialog();
                break;
            case R.id.item_download_all:
                showConfirmMultiDownloadDialog();
                break;
            default:
                Log.e(LOG_UI, "onOptionsItemSelected: invalid option: " + item.getItemId());
        }

        return super.onOptionsItemSelected(item);
    }
}
