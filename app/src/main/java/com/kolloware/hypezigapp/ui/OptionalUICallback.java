package com.kolloware.hypezigapp.ui;

import android.util.Log;

import com.kolloware.hypezigapp.models.Model;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;

public class OptionalUICallback implements UICallback {
    @Override
    public void refresh() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".refresh() called");

        DownloadsRecyclerViewAdapter adapter = Model.getInstance().getDownloadsRecyclerViewAdapter();

        if (adapter != null) {
            adapter.refresh();
        }
    }
}
