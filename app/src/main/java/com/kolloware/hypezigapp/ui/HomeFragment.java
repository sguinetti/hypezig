package com.kolloware.hypezigapp.ui;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.db.queries.QueryStrategy;
import com.kolloware.hypezigapp.db.queries.SortByCategory;
import com.kolloware.hypezigapp.db.queries.SortByDate;
import com.kolloware.hypezigapp.db.queries.SortByLocation;
import com.kolloware.hypezigapp.db.queries.SortByName;
import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.models.filters.DatePreset;
import com.kolloware.hypezigapp.models.filters.EverythingPreset;
import com.kolloware.hypezigapp.models.filters.NextWeekPreset;
import com.kolloware.hypezigapp.models.filters.ThisWeekPreset;
import com.kolloware.hypezigapp.models.filters.TodayPreset;
import com.kolloware.hypezigapp.models.filters.WeekendPreset;
import com.kolloware.hypezigapp.tasks.ReadEventsFromDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;


public class HomeFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private RecyclerViewAdapter adapter;

    private AlertDialog categoriesDialog;

    private SearchView searchView;

    private TextView dateFromText, dateToText;

    String[] queryLabels = null;

    QueryStrategy[] queryStrategies = new QueryStrategy[]{
            new SortByDate(),
            new SortByName(),
            new SortByCategory(),
            new SortByLocation()
    };
    int queryWhich = 0;

    String[] categoryLabels = null;
    Category[] categoryEnums = null;

    private int currentDatePickerDialogId = -1;
    private final static int DATE_PICKER_FROM = 0;
    private final static int DATE_PICKER_TO = 1;

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.");

    String[] datePresetLabels = null;

    DatePreset[] datePresets = new DatePreset[]{
            new TodayPreset(),
            new WeekendPreset(),
            new ThisWeekPreset(),
            new NextWeekPreset(),
            new EverythingPreset()
    };

    int datePresetWhich = 0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        initQueryLabels();
        initCategoryLabels();
        initDatePresetLabels();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateView() called with: inflater = ["
                + inflater + "], container = [" + container + "], savedInstanceState = ["
                + savedInstanceState + "]");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onViewCreated() called with: view = [" + view
                + "], savedInstanceState = [" + savedInstanceState + "]");
        super.onViewCreated(view, savedInstanceState);

        final SwipeRefreshLayout layout = view.findViewById(R.id.refreshLayout);

        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                layout.setRefreshing(false);
            }
        });

        view.findViewById(R.id.datePresetButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDatePresets();
            }
        });

        initDatePickers(view);
        initRecyclerView(view);

        updateDateTextBoxes();

        (new ReadEventsFromDatabase(getActivity().getApplicationContext(), adapter))
                .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void initDatePickers(View inView) {
        Log.d(LOG_UI, getClass().getName() + ".initDatePickers() called with: inView = ["
                + inView + "]");

        dateFromText = inView.findViewById(R.id.dateFromText);
        dateToText = inView.findViewById(R.id.dateToText);

        inView.findViewById(R.id.dateFromText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(Model.getInstance().getDateFrom());

                DatePickerDialog dialogFrom = new DatePickerDialog(
                        getContext(), HomeFragment.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                currentDatePickerDialogId = DATE_PICKER_FROM;

                dialogFrom.show();
            }
        });

        inView.findViewById(R.id.dateToText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(Model.getInstance().getDateTo());

                DatePickerDialog dialogTo = new DatePickerDialog(
                        getContext(), HomeFragment.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH));

                currentDatePickerDialogId = DATE_PICKER_TO;

                dialogTo.show();
            }
        });
    }

    private void initRecyclerView(@NonNull View view) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".initRecyclerView() called with: view = ["
                + view + "]");
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        TextView emptyView = view.findViewById(R.id.emptyView);
        TextView noDownloadersView = view.findViewById(R.id.noDownloaders);
        adapter = new RecyclerViewAdapter(getActivity(), Model.getInstance().getFilteredEvents(),
                recyclerView, emptyView, noDownloadersView);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }


    private AlertDialog buildCategoriesDialog() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".buildCategoriesDialog() called");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.category_filter_show_only);
        builder.setMultiChoiceItems(categoryLabels, null, null);
        builder.setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNeutralButton(R.string.category_filter_all, null);
        builder.setPositiveButton(R.string.dialog_button_okay, null);

        return builder.create();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreateOptionsMenu() called with: menu = ["
                + menu + "], inflater = [" + inflater + "]");
        inflater.inflate(R.menu.main_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.item_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onOptionsItemSelected() called with: item = ["
                + item + "]");

        switch (item.getItemId()) {
            case R.id.item_sort:
                showAlertSortMenu();
                break;
            case R.id.item_filter:
                showAlertCategoryFilter();
                break;
            default:
                Log.e(LOG_UI, "onOptionsItemSelected: invalid option: " + item.getItemId());
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAlertSortMenu() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".showAlertSortMenu() called");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.sorting_action_title);
        builder.setSingleChoiceItems(queryLabels, queryWhich, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(LOG_UI, getClass().getSimpleName() + ".onClick() called with: dialog = ["
                        + dialog + "], which = [" + which + "]");
                Log.i(LOG_UI, "onClick: chosen: " + queryLabels[which]);

                queryWhich = which;
                Model.getInstance().setQueryStrategy(queryStrategies[which]);
                dialog.dismiss();

                (new ReadEventsFromDatabase(getActivity().getApplicationContext(), adapter))
                        .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
        builder.setNeutralButton(getString(R.string.sorting_action_cancel),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addClickListenersToCategoryFilter() {
        categoriesDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListView listView = categoriesDialog.getListView();

                boolean containsUnchecked = false;

                for (int i = 0; i < categoryLabels.length; i++) {
                    Log.v(LOG_UI, "item " + i + " checked? " + listView.isItemChecked(i));
                    if (!listView.isItemChecked(i)) {
                        containsUnchecked = true;
                        break;
                    }
                }

                if (containsUnchecked) {
                    Log.v(LOG_UI, "set all items checked");
                    for (int i = 0; i < categoryLabels.length; i++) {
                        listView.setItemChecked(i, true);
                    }
                }
                else {
                    Log.v(LOG_UI, "set all items unchecked");
                    for (int i = 0; i < categoryLabels.length; i++) {
                        listView.setItemChecked(i, false);
                    }
                }

                applySearchFilter();
            }
        });

        categoriesDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListView listView = categoriesDialog.getListView();

                Set<Category> categoryLabelsSelected = new HashSet<>();

                for (int i = 0; i < categoryLabels.length; i++) {
                    if (listView.isItemChecked(i)) {
                        categoryLabelsSelected.add(categoryEnums[i]);
                    }
                }

                Model.getInstance().getCategoryFilter().setCategories(categoryLabelsSelected);
                Model.getInstance().applyFilter();
                adapter.updateEventsToDisplay(Model.getInstance().getFilteredEvents());
                applySearchFilter();

                categoriesDialog.dismiss();
            }
        });
    }

    private void showAlertCategoryFilter() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".showAlertCategoryFilter() called");
        if (categoriesDialog == null) {
            categoriesDialog = buildCategoriesDialog();
            categoriesDialog.show();
            addClickListenersToCategoryFilter();
        }
        else {
            categoriesDialog.show();
        }
    }

    private void showAlertDatePresets() {
        Log.d(LOG_UI, getClass().getName() + ".showAlertDatePresets() called");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.date_picker_preset_title);
        builder.setSingleChoiceItems(datePresetLabels, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(LOG_UI, getClass().getSimpleName() + ".onClick() called with: dialog = ["
                        + dialog + "], which = [" + which + "]");
                Log.i(LOG_UI, "onClick: chosen: " + datePresetLabels[which]);

                datePresetWhich = which;

                DatePreset chosenPreset = datePresets[which];

                dateFromText.setText(DATE_FORMAT.format(chosenPreset.getDateFrom()));
                dateToText.setText(DATE_FORMAT.format(chosenPreset.getDateTo()));
                Model.getInstance().setDateFrom(chosenPreset.getDateFrom());
                Model.getInstance().setDateTo(chosenPreset.getDateTo());

                Model.getInstance().updateDateFilter();

                dialog.dismiss();

                adapter.updateEventsToDisplay(Model.getInstance().getFilteredEvents());
                applySearchFilter();
            }
        });
        builder.setNeutralButton(getString(R.string.sorting_action_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void applySearchFilter() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".applySearchFilter() called");
        if (searchView != null) {
            adapter.getFilter().filter(searchView.getQuery());
        }
    }

    private void initQueryLabels() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".initQueryLabels() called");

        queryLabels = new String[]{
                getString(R.string.query_label_time),
                getString(R.string.query_label_name),
                getString(R.string.query_label_category),
                getString(R.string.query_label_location)};
    }

    private void initCategoryLabels() {
        Log.d(LOG_UI, getClass().getSimpleName() + ".initQueryLabels() called");

        Map<String, Category> categoryByLabel = UITools.getInstance(getContext())
                .getCategoryByLabelMap();
        Log.d(LOG_UI, "Mapped categories: " + categoryByLabel.toString());


        categoryLabels =  categoryByLabel.keySet().toArray(new String[0]);
        Arrays.sort(categoryLabels);

        List<Category> categoryEnumList = new ArrayList<>();
        for (String forCategoryLabel : categoryLabels) {
            categoryEnumList.add(categoryByLabel.get(forCategoryLabel));
        }
        categoryEnums = categoryEnumList.toArray(new Category[0]);

        Log.d(LOG_UI, "Category labels: " + categoryLabels.toString());
    }

    private void initDatePresetLabels() {
        datePresetLabels = new String[] {
                getString(R.string.date_picker_preset_today),
                getString(R.string.date_picker_preset_weekend),
                getString(R.string.date_picker_preset_this_week),
                getString(R.string.date_picker_preset_next_week),
                getString(R.string.date_picker_preset_everything)
        };
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onDateSet() called with: view = ["
                + view + "], year = [" + year + "], month = [" + month + "], dayOfMonth = ["
                + dayOfMonth + "]");

        switch (currentDatePickerDialogId) {
            case DATE_PICKER_FROM:
                Calendar newFromDate = Calendar.getInstance();
                newFromDate.set(Calendar.YEAR, year);
                newFromDate.set(Calendar.MONTH, month);
                newFromDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Model.getInstance().setDateFrom(newFromDate.getTime());
                if (Model.getInstance().getDateTo().compareTo(newFromDate.getTime()) < 0) {
                    Model.getInstance().setDateTo(newFromDate.getTime());
                }
                break;
            case DATE_PICKER_TO:
                Calendar newToDate = Calendar.getInstance();
                newToDate.set(Calendar.YEAR, year);
                newToDate.set(Calendar.MONTH, month);
                newToDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                Model.getInstance().setDateTo(newToDate.getTime());
                if (newToDate.getTime().compareTo(Model.getInstance().getDateFrom()) < 0) {
                    Model.getInstance().setDateFrom(newToDate.getTime());
                }
                break;
            default:
                Log.e(LOG_UI, "onDateSet: Invalid date picker ID");
        }

        updateDateTextBoxes();
        Model.getInstance().updateDateFilter();
        adapter.updateEventsToDisplay(Model.getInstance().getFilteredEvents());
        applySearchFilter();
    }

    private void updateDateTextBoxes() {
        dateFromText.setText(DATE_FORMAT.format(Model.getInstance().getDateFrom()));
        dateToText.setText(DATE_FORMAT.format(Model.getInstance().getDateTo()));
    }
}
