package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public abstract class AbstractFacebookScraper implements Scraper {
    private static final int THREAD_ID = AbstractFacebookScraper.class.getName().hashCode();

    protected Downloader downloader;

    private static final String FACEBOOK_PAGE_URL = "https://www.facebook.com/api/graphql/";

    public static final String DOC_ID_UPCOMING_EVENTS ="2455863461165494";
    public static final String DOC_ID_UPCOMING_EVENTS_PAGINATION ="2464276676984576";
    public static final String DOC_ID_EVENT_DETAIL_TEXT = "1640160956043533";

    private static final String KEY_RID = "&rid=";

    private Iterator<JSONObject> eventIterator;
    private int currentEvent = 0;
    private int numEvents = 1;

    private List<Event> results = new ArrayList<>();

    public AbstractFacebookScraper(Downloader downloader) throws Exception {
        this.downloader = downloader;
        TrafficStats.setThreadStatsTag(THREAD_ID);
    }

    abstract protected String getFacebookPageUrl();
    abstract protected Category getDefaultCategory();

    public void init() throws Exception {
        String pageId = getFacebookPageID(getFacebookPageUrl());

        JSONArray upcomingEvents = getUpcomingEvents(pageId);

        numEvents = upcomingEvents.length();
        Log.v(LOG_NET, numEvents + " events found");

        List<JSONObject> eventList = new ArrayList<>(numEvents);
        for (int i = 0; i < numEvents; i++) {
            eventList.add(upcomingEvents.getJSONObject(i));
        }
        eventIterator = eventList.iterator();
    }


    private String getFacebookPageID(String inURL) throws Exception {
        Connection connection = Jsoup.connect(inURL);
        connection.maxBodySize(0);
        Document doc = connection.timeout(2 * 60 * 1000)
                .get();

        Elements allLinks = doc.select("a");

        for (Element forElement : allLinks) {
            String href = forElement.attr("href");

            if (href.contains(KEY_RID)) {
                String rid = href.substring(href.indexOf(KEY_RID) + KEY_RID.length());
                if (rid.contains("&")) {
                    rid = rid.substring(0, rid.indexOf("&"));
                }

                return rid;
            }
        }

        return null;
    }

    private JSONObject getUpcomingEventsWithCursor(String inPageId, String inCursor) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".getUpcomingEventsWithCursor() called with: inPageId = ["
                + inPageId + "], inCursor = [" + inCursor + "]");

        Connection connection = Jsoup.connect(FACEBOOK_PAGE_URL);
        connection.maxBodySize(0);

        String requestName = "PageEventsTabUpcomingEventsCardRendererQuery";
        String docId = DOC_ID_UPCOMING_EVENTS;

        Map<String, String> requestVariables = new HashMap<>();
        requestVariables.put("pageID", inPageId);
        if (inCursor != null) {
            requestVariables.put("count", "30");
            requestVariables.put("cursor", inCursor);

            requestName = "PageEventsTabUpcomingEventsCardPaginationQuery";
            docId = DOC_ID_UPCOMING_EVENTS_PAGINATION;
        }

        JSONObject variables = new JSONObject(requestVariables);
        Log.v(LOG_NET, "variables: " + variables);

        Document doc = connection.timeout(2 * 60 * 1000)
                .data("fb_api_req_friendly_name", requestName)
                .data("variables", variables.toString())
                .data("doc_id", docId)
                .post();

        String jsonText = doc.body().text();
        JSONObject response = new JSONObject(jsonText);

        return response.getJSONObject("data")
                .getJSONObject("page")
                .getJSONObject("upcoming_events");
    }


    private JSONArray getUpcomingEvents(String inPageId) throws Exception {

        JSONArray localResult = new JSONArray();
        String cursor = null;
        boolean hasMoreEvents;

        do {
            JSONObject events = getUpcomingEventsWithCursor(inPageId, cursor);

            JSONArray edges = events.getJSONArray("edges");
            for (int i = 0; i < edges.length(); i++) {
                localResult.put(edges.get(i));
            }

            JSONObject pageInfo = events.getJSONObject("page_info");

            if (pageInfo.getBoolean("has_next_page")) {
                Log.v(LOG_NET, "More data: " + pageInfo);
                cursor = pageInfo.getString("end_cursor");
                hasMoreEvents = true;
            }
            else {
                Log.v(LOG_NET, "No more data found");
                hasMoreEvents = false;
            }
        } while (hasMoreEvents);

        return localResult;
    }

    @Override
    public DownloadStatus getResult() {
        return DownloadStatus.SUCCESS;
    }

    @Override
    public List<Event> getEvents() {
        return results;
    }

    @Override
    public boolean hasNext() {
        return eventIterator.hasNext();
    }

    @Override
    public void scrapeNext() throws Exception {
        JSONObject forJSONObject = eventIterator.next();
        currentEvent++;

        JSONObject node = forJSONObject.getJSONObject("node");

        String title = forJSONObject.getJSONObject("node").getString("name");
        String subtitle = null;
        String details = null;
        Date date = new Date(1000 * node.getLong("startTimestampForDisplay"));

        String locationName = "t.b.a.", locationURL = null;
        List<String> tags = new ArrayList<>();
        String imageURL = null;
        Category category = getDefaultCategory();
        String providerName = "facebook.com";
        String providerId = node.getString("eventID");
        String providerCategory = null;
        Integer downloaderId = downloader.downloaderId;

        String eventURL = "https://www.facebook.com/events/" + providerId;

        Object eventPlace = node.get("event_place");

        if (eventPlace instanceof JSONObject) {
            JSONObject eventPlaceJSON = (JSONObject) eventPlace;
            locationName = eventPlaceJSON.optString("name", "t.b.a.");
            locationURL = eventPlaceJSON.optString("url", null);
        }

        Event newEvent = new Event(title, subtitle, details, date, locationName,
                tags, imageURL, category, providerName, providerId, providerCategory, downloaderId);

        newEvent.locationURL = locationURL;
        newEvent.eventURL = eventURL;

        results.add(newEvent);
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentEvent / numEvents);
    }


    private String getDescription(Event inEvent) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".getDescription() called with: inEvent = [" + inEvent + "]");

        Connection connection = Jsoup.connect(FACEBOOK_PAGE_URL);
        connection.maxBodySize(0);

        String requestName = "EventsEventDetailsCardRendererQuery";
        String docId = DOC_ID_EVENT_DETAIL_TEXT;

        Map<String, String> requestVariables = new HashMap<>();
        requestVariables.put("eventID", inEvent.providerId);

        JSONObject variables = new JSONObject(requestVariables);
        Log.v(LOG_NET, "variables: " + variables);

        Document doc = connection.timeout(2 * 60 * 1000)
                .data("fb_api_req_friendly_name", requestName)
                .data("variables", variables.toString())
                .data("doc_id", docId)
                .post();

        String jsonText = doc.body().text();
        JSONObject response = new JSONObject(jsonText);

        return response.getJSONObject("data")
                .getJSONObject("event")
                .getJSONObject("details")
                .getString("text");
    }

    private String getImageURL(Event inEvent) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".getImageURL() called with: inEvent = [" + inEvent + "]");

        String url = "https://www.facebook.com/events/" + inEvent.providerId;
        Log.i(LOG_NET, "Read URL: " + url);

        Connection connection = Jsoup.connect(url);
        connection.maxBodySize(0);
        Document doc = connection.timeout(2 * 60 * 1000)
                .get();


        Elements elements = doc.select("img");

        if (elements.size() > 0) {
            return elements.first().attr("src");
        }

        return null;
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".updateEvent() called with: inOutEvent = [" + inOutEvent + "]");

        if (inOutEvent.details == null || inOutEvent.details.isEmpty()) {
            inOutEvent.details = getDescription(inOutEvent);
            inOutEvent.imageURL = getImageURL(inOutEvent);
            return true;
        }
        else {
            return false;
        }
    }
}
