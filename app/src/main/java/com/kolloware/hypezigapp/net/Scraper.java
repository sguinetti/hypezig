package com.kolloware.hypezigapp.net;

import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Event;

import java.util.List;

public interface Scraper {

    DownloadStatus getResult();
    List<Event> getEvents();

    void init() throws Exception;
    boolean hasNext();
    void scrapeNext() throws Exception;
    int getProgress();

    boolean updateEvent(Event inOutEvent) throws Exception;
}
