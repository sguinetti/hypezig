package com.kolloware.hypezigapp.net;

import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Downloader;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class ConfigurableICalScraper extends AbstractICalScraper {

    public ConfigurableICalScraper(Downloader downloader) {
        super(downloader);
    }

    @Override
    protected String getIcalURL() {

        try {
            return downloader.getProperties().getString(Downloader.PROPERTY_URL);
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.e(LOG_NET, Log.getStackTraceString(e));
        }

        return null;
    }

    @Override
    protected String getProviderName() {
        return downloader.getTitle();
    }

    @Override
    protected Category getDefaultCategory() {
        return Category.MISC;
    }
}
