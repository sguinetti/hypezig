package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.util.StupidIcalReader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

abstract public class AbstractICalScraper implements Scraper {

    private static final int THREAD_ID = AbstractICalScraper.class.getName().hashCode();

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
    private static final String DEFAULT_TIME_SUFFIX = "T000000";

    protected Downloader downloader;
    private Iterator<JSONObject> elementIterator;
    private int currentEvent = 0;
    private int numEvents = 1;
    private DownloadStatus status = DownloadStatus.SUCCESS;

    private List<Event> result = new LinkedList<>();


    public AbstractICalScraper(Downloader downloader) {
        this.downloader = downloader;
    }

    @Override
    public DownloadStatus getResult() {
        return status;
    }

    @Override
    public List<Event> getEvents() {
        return result;
    }

    private String findKeyStartingWithPattern(String inStartsWithPattern, JSONObject inObject) {
        String forKey;
        Iterator<String> keyIterator = inObject.keys();

        while (keyIterator.hasNext()) {
            forKey = keyIterator.next();
            if (forKey.startsWith(inStartsWithPattern)) {
                return forKey;
            }
        }

        return null;
    }

    abstract protected String getIcalURL();
    abstract protected String getProviderName();
    abstract protected Category getDefaultCategory();


    @Override
    public void init() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".init() called");
        TrafficStats.setThreadStatsTag(THREAD_ID);

        JSONObject result = StupidIcalReader.getIcalAsJSON(getIcalURL());
        final JSONArray events = result.getJSONArray("VCALENDAR").getJSONObject(0).getJSONArray("VEVENT");

        numEvents = events.length();

        elementIterator = new Iterator<JSONObject>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < events.length();
            }

            @Override
            public JSONObject next() {
                try {
                    JSONObject result = (JSONObject) events.get(i);
                    i++;
                    return result;
                }
                catch (Exception e) {
                    Log.e(LOG_NET, "Error: " + e.getMessage());
                    Log.e(LOG_NET, Log.getStackTraceString(e));
                }
                return null;
            }
        };
    }

    @Override
    public boolean hasNext() {
        return elementIterator.hasNext();
    }

    @Override
    public void scrapeNext() throws Exception {
        currentEvent++;
        JSONObject forEvent = elementIterator.next();


        String eventTitle = forEvent.getString("SUMMARY");
        String eventDetails = forEvent.optString("DESCRIPTION");
        String providerId = forEvent.getString("UID");
        String locationName = forEvent.optString("LOCATION");
        String eventURL = forEvent.optString("URL");
        String imageURL = forEvent.optString(
                findKeyStartingWithPattern("ATTACH;FMTTYPE=image", forEvent));

        String categories = forEvent.optString("CATEGORIES");
        List<String> tags = new LinkedList<>();
        if (categories != null) {
            String[] categoriesArray = categories.split(",");
            tags = Arrays.asList(categoriesArray);
        }

        String date = forEvent.getString(
                findKeyStartingWithPattern("DTSTART", forEvent));
        if (!date.contains("T")) { date += DEFAULT_TIME_SUFFIX; }
        Date eventDate = DATE_FORMAT.parse(date);

        Log.v(LOG_NET, "eventTitle: " + eventTitle);
        Log.v(LOG_NET, "eventDetails: " + eventDetails);
        Log.v(LOG_NET, "providerId: " + providerId);
        Log.v(LOG_NET, "locationName: " + locationName);
        Log.v(LOG_NET, "eventURL: " + eventURL);
        Log.v(LOG_NET, "imageURL: " + imageURL);
        Log.v(LOG_NET, "tags: " + tags);
        Log.v(LOG_NET, "eventDate: " + eventDate);

        Event newEvent = new Event(eventTitle,
                null,
                eventDetails,
                eventDate,
                locationName,
                tags,
                imageURL,
                getDefaultCategory(),
                getProviderName(),
                providerId,
                categories,
                downloader.downloaderId);

        newEvent.eventURL = eventURL;

        result.add(newEvent);
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentEvent / numEvents);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }
}
