package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class Sachsenpunk implements Scraper {
    private static final int THREAD_ID = Sachsenpunk.class.getName().hashCode();
    private static final String SACHSENPUNK_URL = "http://sachsenpunk.de/";
    private static final String PROVIDER_NAME = "sachsenpunk.de";

    private static final Pattern REGEX_DATE = Pattern.compile("(\\d{2})\\.(\\d{2})\\. \\(.{2}\\)");
    private static final String DIVIDER = " - ";
    private static final String PREFIX_LEIPZIG = "Leipzig" + DIVIDER;
    private static final String SPECIAL_TIME = " Uhr!" + DIVIDER;
    private static final String DEFAULT_TIME = "20:00";
    private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat(
            "dd.MM.yyyy HH:mm");

    private List<Event> result = new ArrayList<>();
    private Map<String, String> locationUrlByName;

    private Downloader downloader;
    private DownloadStatus status = DownloadStatus.SUCCESS;
    private int numLines = 1;
    private int currentLine = 0;
    private Iterator<Node> iterator = null;

    private String currentYear = "2019";
    private String currentDate = "01.01.";

    public Sachsenpunk(Downloader downloader) throws Exception {
        this.downloader = downloader;
    }

    public void init() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".init() called");

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Log.i(LOG_NET, "fetchEvents: SACHSENPUNK_URL to be read = " + SACHSENPUNK_URL);
        Document doc = Jsoup.parse(new URL(SACHSENPUNK_URL).openStream(),
                "ISO-8859-1", SACHSENPUNK_URL);

        Elements elements = doc.select("table tbody tr td:last-of-type font font");

        List<Node> childNodes = elements.first().childNodes();
        Log.d(LOG_NET, "Result: " + childNodes);

        Log.d(LOG_NET, "Count: " + childNodes.size());

        iterator = childNodes.iterator();
        numLines = childNodes.size();

        initLocationUrls(doc);
    }

    private void initLocationUrls(Document doc) {
        locationUrlByName = new HashMap<>();

        Elements elements = doc.select("table tbody tr td:first-of-type font font font font a");

        for (Element forElement : elements) {
            // <a href="http://www.conne-island.de/" target="_blank">Leipzig - Conne Island</a>
            String elementText = forElement.text();

            if (elementText.startsWith(PREFIX_LEIPZIG)) {
                String locationName = elementText.substring(PREFIX_LEIPZIG.length());
                String url = forElement.attr("href");
                locationUrlByName.put(locationName, url);
            }
        }

        Log.d(LOG_NET, "location map: " + locationUrlByName);
    }

    @Override
    public DownloadStatus getResult() {
        return status;
    }

    @Override
    public List<Event> getEvents() {
        return result;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    private void processMonthIndicator(String inString) {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processMonthIndicator() called with: inString = [" + inString + "]");
        currentYear = inString.split(" ")[2];
        Log.d(LOG_NET, "new year: " + currentYear);
    }

    private void processDateIndicator(String inString) {
        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processDateIndicator() called with: inString = [" + inString + "]");

        currentDate = inString.substring(0,7);
        Log.d(LOG_NET, "new date: " + currentDate);
    }

    private void processEvent(String inString) throws Exception {
        // Leipzig - UT Connewitz - 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film

        Log.d(LOG_NET, getClass().getSimpleName()
                + ".processEvent() called with: inString = [" + inString + "]");

        // UT Connewitz - 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film
        String restOfLine = inString.substring(PREFIX_LEIPZIG.length());

        // UT Connewitz
        String locationName = restOfLine.substring(0, restOfLine.indexOf(DIVIDER));
        Log.d(LOG_NET, "locationName: " + locationName);

        // 20:30 Uhr! - "Heldenstadt Anders"-Festival-Film
        String eventText = restOfLine.substring(restOfLine.indexOf(DIVIDER) + DIVIDER.length());

        String eventTitle = "";
        String eventSubtitle = "";
        String eventTime = "";

        if (eventText.contains(SPECIAL_TIME)) {
            Log.d(LOG_NET, "Special time info: " + eventText);

            int position = eventText.indexOf(SPECIAL_TIME);

            String specialTime;

            if (position <= 5) {
                specialTime = eventText.substring(0, eventText.indexOf(SPECIAL_TIME));
            }
            else {
                String additionalInfo = eventText.substring(0, eventText.indexOf(DIVIDER));
                eventSubtitle = additionalInfo;
                specialTime = eventText.substring(eventText.indexOf(DIVIDER) + DIVIDER.length(),
                        eventText.indexOf(SPECIAL_TIME));
            }

            if (!specialTime.contains(":")) {
                specialTime += ":00";
            }

            eventTitle = eventText.substring(eventText.indexOf(SPECIAL_TIME) + SPECIAL_TIME.length());
            eventTime = specialTime;
        }
        else {
            Log.d(LOG_NET, "Default time: " + eventText);

            eventTitle = eventText;
            eventTime = DEFAULT_TIME;
        }

        Log.d(LOG_NET, "title: " + eventTitle + ", time: " + eventTime);

        Date eventDate = INPUT_DATE_FORMAT.parse(currentDate + currentYear + " " + eventTime);

        Event newEvent = new Event(eventTitle,
                eventSubtitle,
                "",
                eventDate,
                locationName,
                new ArrayList<String>(),
                null,
                Category.MUSIC,
                PROVIDER_NAME,
                inString,
                null,
                downloader.downloaderId);

        if (locationUrlByName.containsKey(locationName)) {
            newEvent.locationURL = locationUrlByName.get(locationName);
        }

        result.add(newEvent);
    }

    @Override
    public void scrapeNext() throws Exception {
        Node forNode = iterator.next();
        currentLine++;

        if (!(forNode instanceof TextNode)) {
            return;
        }

        TextNode textNode = (TextNode) forNode;

        String forLine = textNode.text().trim();

        if (forLine.isEmpty()) {
            return;
        }

        if (forLine.startsWith("---")) {
            // ------------- September 2020 -------------
            processMonthIndicator(forLine);
        }
        else if (REGEX_DATE.matcher(forLine).matches()) {
            // 05.09. (Sa)
            processDateIndicator(forLine);
        }
        else if (forLine.startsWith(PREFIX_LEIPZIG)) {
            // Leipzig - tba. - "Sternburg Fanfest - Deep Shining High + mehr Bands tba.
            processEvent(forLine);
        }
        else {
            Log.d(LOG_NET, "Ignoring: " + currentLine + ": " + forLine);
        }
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentLine / numLines);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }
}
