package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.util.JSoupUtil;
import com.kolloware.hypezigapp.util.NestedIterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class TrichterScraper implements Scraper {

    private static final int THREAD_ID = TrichterScraper.class.getName().hashCode();

    private final static String BASE_URL = "https://trichter.cc";
    private static final SimpleDateFormat EVENT_MONTH = new SimpleDateFormat(
            "MMMM", Locale.GERMANY);
    private static final SimpleDateFormat DAY_MONTH_TIME = new SimpleDateFormat(
            "dd.MM. HH:mm");
    private static final SimpleDateFormat FULL_DATE = new SimpleDateFormat(
            "dd.MM.yyyy");
    private final static int NUM_MONTHS_TO_SCRAPE = 3;
    private final static Map<String, Category> CATEGORY_BY_LABEL = new HashMap<String, Category>() {{
        put("Küche", Category.FOOD);
        put("Küfa", Category.FOOD);
        put("Brunch", Category.FOOD);
        put("Ausstellung", Category.ART);
        put("Exhibition", Category.ART);
        put("Film", Category.CINEMA);
        put("Lesung", Category.LECTURES);
        put("Kunst", Category.ART);
        put("Café", Category.FOOD);
    }};

    private final static String PROVIDER_NAME = "trichter.cc";

    private Downloader downloader;
    private Iterator<Element> elementIterator;
    private int currentEvent = 0;
    private int numEvents = 1;
    private DownloadStatus status = DownloadStatus.SUCCESS;

    private List<Event> result = new LinkedList<>();


    public TrichterScraper(Downloader downloader) {
        this.downloader = downloader;
    }

    @Override
    public DownloadStatus getResult() {
        return status;
    }

    @Override
    public List<Event> getEvents() {
        return result;
    }

    private String getURLByDate(Calendar inCalendar) {
        int year = inCalendar.get(Calendar.YEAR);
        String monthName = EVENT_MONTH.format(inCalendar.getTime());

        return BASE_URL + "/" + year + "/" + monthName.toLowerCase() + ".html";
    }

    @Override
    public void init() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".init() called");
        TrafficStats.setThreadStatsTag(THREAD_ID);

        Calendar forDate = Calendar.getInstance();
        forDate.set(Calendar.DAY_OF_MONTH, 1);

        List<Iterator<Element>> iterators = new LinkedList<>();
        numEvents = 0;

        for (int i = 0; i < NUM_MONTHS_TO_SCRAPE; i++) {
            String url = getURLByDate(forDate);
            Log.v(LOG_NET, "URL: " + url);

            Document document = JSoupUtil.getDocumentFromURL(url);
            Elements elements = document.select(".day");

            numEvents += elements.size();
            iterators.add(elements.iterator());

            forDate.add(Calendar.MONTH, 1);
        }

        elementIterator = new NestedIterator<>(iterators);
    }

    @Override
    public boolean hasNext() {
        return elementIterator.hasNext();
    }

    private Category guessCategoryByEventTitleOrDescription(String inTitle, String inDescription) {
        for (Map.Entry<String, Category> forEntry : CATEGORY_BY_LABEL.entrySet()) {
            if (inTitle.contains(forEntry.getKey())) {
                return forEntry.getValue();
            }

            if (inDescription != null && inDescription.contains(forEntry.getKey())) {
                return forEntry.getValue();
            }
        }

        return Category.MISC;
    }

    @Override
    public void scrapeNext() throws Exception {
        currentEvent++;
        Element forElement = elementIterator.next();

        Calendar forCalendar = Calendar.getInstance();
        forCalendar.set(Calendar.DAY_OF_MONTH, 1);
        int thisYear = forCalendar.get(Calendar.YEAR);
        int thisMonth = forCalendar.get(Calendar.MONTH);

        String smallText = forElement.selectFirst(".header.columns small").text();
        String eventDate = smallText.substring(smallText.indexOf(" ") + 1);

        Elements entries = forElement.select(".columns.entry");
        for (Element forEntry : entries) {
            String eventTitle = forEntry.selectFirst("h4").text();

            String eventTime = forEntry.select(".times-content").text();
            int posDash = eventTime.indexOf(" - ");
            String startTime = eventTime.substring(0, (posDash > 0) ? posDash : eventTime.length());
            Calendar eventDateTime = Calendar.getInstance();
            eventDateTime.setTime((DAY_MONTH_TIME.parse(eventDate + " " + startTime)));
            int eventMonth = eventDateTime.get(Calendar.MONTH);
            if (eventMonth >= thisMonth) {
                eventDateTime.set(Calendar.YEAR, thisYear);
            }
            else {
                eventDateTime.set(Calendar.YEAR, thisYear + 1);
            }

            Element locationElement = forEntry.selectFirst(".location a");
            String locationName, locationURL = null;

            if (locationElement != null) {
                locationName = locationElement.text();
                locationURL = locationElement.attr("href");
            }
            else {
                locationName = forEntry.selectFirst(".location span").text();
            }

            String eventURL = BASE_URL + forEntry.selectFirst("h4 a").attr("href");

            Element teaser = forEntry.selectFirst(".teaser");
            String eventDescription = (teaser != null) ? teaser.text() : null;
            Category eventCategory = guessCategoryByEventTitleOrDescription(eventTitle,
                    eventDescription);

            String providerId = FULL_DATE.format(eventDateTime.getTime()) + " " + eventTitle;

            Log.v(LOG_NET, "title: " + eventTitle);
            Log.v(LOG_NET, "category: " + eventCategory);
            Log.v(LOG_NET, "eventDay: " + eventDateTime.getTime());
            Log.v(LOG_NET, "locationName: " + locationName);
            Log.v(LOG_NET, "locationURL: " + locationURL);
            Log.v(LOG_NET, "eventURL: " + eventURL);
            Log.v(LOG_NET, "eventDescription: " + eventDescription);
            Log.v(LOG_NET, "providerId: " + providerId);

            Event newEvent = new Event(eventTitle,
                    null,
                    eventDescription,
                    eventDateTime.getTime(),
                    locationName,
                    new LinkedList<String>(),
                    null,
                    eventCategory,
                    PROVIDER_NAME,
                    providerId,
                    null,
                    downloader.downloaderId
            );

            newEvent.locationURL = locationURL;
            newEvent.eventURL = eventURL;

            result.add(newEvent);
        }
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentEvent / numEvents);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        Document document = JSoupUtil.getDocumentFromURL(inOutEvent.eventURL);

        Element eventImage = document.selectFirst(".eventimage");
        if (eventImage != null) {
            String styleText = eventImage.attr("style");
            int posStart = styleText.indexOf("url('");
            int posEnd = styleText.indexOf("')");
            String url = styleText.substring(posStart + 5, posEnd);

            inOutEvent.imageURL = BASE_URL + url;

            Log.v(LOG_NET, "image URL: " + url);
        }

        Element description = document.selectFirst(".description");
        if (description != null) {
            inOutEvent.details = description.text();
        }

        return true;
    }
}
