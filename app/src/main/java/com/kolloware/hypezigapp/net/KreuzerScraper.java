package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class KreuzerScraper implements Scraper {
    private static final int THREAD_ID = 40000;

    private static final String PROVIDER_NAME = "kreuzer-leipzig.de";
    private static final SimpleDateFormat INPUT_DATE_FORMAT = new SimpleDateFormat(
            "dd.MM.yyyy HH:mm");
    private static final SimpleDateFormat ID_DATE_FORMAT = new SimpleDateFormat(
            "yyyyMMddHHmm");

    private final Pattern patternDate = Pattern.compile("(\\d{2})\\.(\\d{2})\\.");
    private final Pattern patternTime = Pattern.compile("(\\d{2})\\:(\\d{2})");
    private final String thisYear = (new SimpleDateFormat("yyyy"))
            .format(Calendar.getInstance().getTime());

    private Iterator<Element> elementIterator;
    private int numEvents = 1;
    private int eventCounter = 0;

    private List<Event> localResult = new ArrayList<>();

    private Downloader downloader;
    private DownloadStatus localDownloadStatus = DownloadStatus.RUNNING;

    public KreuzerScraper(Downloader downloader) throws Exception {
        this.downloader = downloader;
    }

    private final static Map<String, Category> CATEGORY_MAPPING = new HashMap<String, Category>() {{
        put("Theater", Category.STAGE);
        put("Kino", Category.CINEMA);
        put("Show", Category.STAGE);
        put("Party", Category.PARTY);
        put("Musik", Category.MUSIC);
        put("Clubbing", Category.PARTY);
        put("Tanzen", Category.PARTY);
        put("Kunst", Category.ART);
        put("Literatur", Category.LECTURES);
        put("Vorträge & Diskussionen", Category.LECTURES);
        put("etc.", Category.MISC);
        put("Kinder & Familie", Category.FAMILY);
        put("Umland", Category.MISC);
        put("Gastro-Events", Category.FOOD);
        put("Lokale Radios", Category.CULTURE);
        put("Natur & Umwelt", Category.MISC);
    }};

    private static Category getCategoryByString(String inCategoryName) {
        Category localResult = CATEGORY_MAPPING.get(inCategoryName);
        if (localResult == null) {
            Log.e(LOG_NET, "No mapping for category: " + inCategoryName);
            return Category.MISC;
        }
        return localResult;
    }

    public void init() throws Exception {
        Log.d(LOG_NET, KreuzerScraper.class.getName() + ".init() called");

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Date today = Calendar.getInstance().getTime();

        Calendar calendarNextMonth = Calendar.getInstance();
        calendarNextMonth.add(Calendar.MONTH, 2);
        Date nextMonth = calendarNextMonth.getTime();

        SimpleDateFormat searchDateFormat = new SimpleDateFormat("dd.MM.yyyy");

        try {
            String url = "https://kreuzer-leipzig.de/termine/"
                    + "?datumVon="
                    + searchDateFormat.format(today)
                    + "&datumBis="
                    + searchDateFormat.format(nextMonth);

            Log.i(LOG_NET, "init: URL to be read = " + url);

            Connection connection = Jsoup.connect(url);
            connection.maxBodySize(0);
            Document doc = connection.timeout(2 * 60 * 1000).get();

            Elements events = doc.select("article.termin");
            Log.i(LOG_NET, "fetchEvents: " + events.size() + " events found.");

            numEvents = events.size();

            elementIterator = events.iterator();
        }
        catch(Exception e) {
            Log.e(LOG_NET, "init: ", e);
            throw(e);
        }
    }

    @Override
    public DownloadStatus getResult() { return localDownloadStatus; }

    @Override
    public List<Event> getEvents() { return localResult; }

    @Override
    public boolean hasNext() {
        if (elementIterator.hasNext()) {
            return true;
        }
        else {
            if (localDownloadStatus == DownloadStatus.RUNNING) {
                localDownloadStatus = DownloadStatus.SUCCESS;
            }
            return false;
        }
    }

    @Override
    public void scrapeNext() throws Exception {
        Log.d(LOG_NET, "fetchEvents: Processing event " + eventCounter + "...");

        try {
            eventCounter++;

            Element event = elementIterator.next();

            Elements titleElement = event.select("h2");
            Elements tagElements = titleElement.select("strong");


            String title;
            Set<String> tags = new HashSet<>();

            if (tagElements.size() > 0) {
                title = titleElement.first().textNodes().get(tagElements.size()).text().trim();
                for (Element forElement : tagElements) {
                    tags.add(forElement.text());
                }
            }
            else {
                title = event.select("h2").text();
            }

            String subtitle = event.select("p.details").text();
            Elements moreText = event.select(".moreText p");
            String details = moreText.text();

            Element whenAndWhere = event.select(".whenAndWhere").first();

            if (whenAndWhere == null) {
                Exception e = new Exception("No .whenAndWhere found");

                Log.e(LOG_NET, "fetchEvents: No .whenAndWhere found!", e);
                Log.i(LOG_NET, "fetchEvents: current event: " + event);

                throw (e);
            }
            Elements locationTimes = whenAndWhere.children();

            String imageURL = event.select("img").attr("src");
            imageURL = !imageURL.isEmpty() ? "https://kreuzer-leipzig.de" + imageURL : null;

            String category = event.previousElementSiblings().select("aside h3").first().text();

            for (int i = 0; i < locationTimes.size(); i+=2) {

                Element locationElement = locationTimes.get(i);
                String locationName = locationElement.text();
                String locationURL = locationElement.attr("href");

                Element timesElement = locationTimes.get(i + 1);

                for (Element time : timesElement.select("li")) {
                    String timeLabel = time.text();
                    String dateAsString = "";
                    Matcher m = patternDate.matcher(timeLabel);

                    while (m.find()) {
                        dateAsString = m.group() + thisYear;
                    }

                    m = patternTime.matcher(timeLabel);

                    while (m.find()) {
                        Date eventDate = INPUT_DATE_FORMAT.parse(dateAsString + " "
                                + m.group());

                        String providerId = event.attr("id") + "_"
                                + ID_DATE_FORMAT.format(eventDate);

                        Category categoryEnum = getCategoryByString(category);
                        tags.add(category);

                        Event newEvent = new Event(title, subtitle, details, eventDate,
                                locationName, new ArrayList<>(tags), imageURL, categoryEnum, PROVIDER_NAME,
                                providerId, category, downloader.downloaderId);

                        newEvent.locationURL = locationURL.isEmpty() ? null :
                                "https://kreuzer-leipzig.de" + locationURL;

                        Log.d(LOG_NET, "fetchEvents: new event created: " + newEvent);

                        localResult.add(newEvent);
                    }
                }
            }
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error while scraping: " + e.getMessage());
            e.printStackTrace();
            localDownloadStatus = DownloadStatus.ERROR;
            throw(e);
        }
    }

    @Override
    public int getProgress() {
        return  (int) (100.0 * eventCounter / numEvents);
    }

    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }
}
