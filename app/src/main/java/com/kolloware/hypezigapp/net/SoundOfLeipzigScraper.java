package com.kolloware.hypezigapp.net;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.Downloader;

public class SoundOfLeipzigScraper extends AbstractFacebookScraper {

    public SoundOfLeipzigScraper(Downloader downloader) throws Exception {
        super(downloader);
    }

    @Override
    protected String getFacebookPageUrl() {
        return "https://m.facebook.com/sndoflpzg/";
    }

    @Override
    protected Category getDefaultCategory() {
        return Category.PARTY;
    }
}
