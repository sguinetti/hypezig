package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.util.JSoupUtil;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class FrauenkulturScraper implements Scraper {
    private static final int THREAD_ID = FrauenkulturScraper.class.getName().hashCode();

    private final static String BASE_URL = "https://www.frauenkultur-leipzig.de";
    private final static String CALENDAR_URL = "https://www.frauenkultur-leipzig.de/programm/";
    private final static Map<String, Category> CATEGORY_BY_LABEL = new HashMap<String, Category>() {{
        put("GEPSRÄCHSRUNDE", Category.LECTURES);
        put("VORTRAG", Category.LECTURES);
        put("WORKSHOP", Category.CULTURE);
        put("PARTY", Category.PARTY);
        put("LITERARISCH", Category.LECTURES);
        put("AUSSTELLUNG", Category.ART);
        put("KONZERT", Category.MUSIC);
    }};
    private final static String LOCATION_NAME = "Frauenkultur";
    private final static String PROVIDER_NAME = "frauenkultur-leipzig.de";

    private Downloader downloader;
    private Iterator<Element> elementIterator;
    private int currentEvent = 0;
    private int numEvents = 1;
    private DownloadStatus status = DownloadStatus.SUCCESS;

    private List<Event> result = new LinkedList<>();


    private final static SimpleDateFormat DATE_FORMAT_DAY_TIME = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public FrauenkulturScraper(Downloader downloader) {
        this.downloader = downloader;
    }

    @Override
    public DownloadStatus getResult() {
        return status;
    }

    @Override
    public List<Event> getEvents() {
        return result;
    }

    @Override
    public void init() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".init() called");

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Document doc = JSoupUtil.getDocumentFromURL(CALENDAR_URL);

        Elements elements = doc.select(".event");

        numEvents = elements.size();
        elementIterator = elements.iterator();
    }

    @Override
    public boolean hasNext() {
        return elementIterator.hasNext();
    }

    @Override
    public void scrapeNext() throws Exception {
        Element forElement = elementIterator.next();

        Calendar today = Calendar.getInstance();
        String thisYear = "" + today.get(Calendar.YEAR);

        Elements heading = forElement.select(".event-title span");
        String eventDay = heading.first().text();
        String title = heading.last().text();
        String providerId = heading.text();

        String imageURL = null;
        Elements imageElements = forElement.select(".event-description img");
        if (imageElements.size() > 0) {
            imageURL = BASE_URL + imageElements.first().attr("src");
        }

        String dayAndTime = forElement.select(".event-description .event-day-and-time").text();
        int posTimeStart = dayAndTime.indexOf("|") + 1;
        int posBis = dayAndTime.indexOf(" bis ");
        int posUhr = dayAndTime.indexOf(" Uhr");
        int posTimeEnd = (posBis > 0) ? posBis : ((posUhr > 0) ? posUhr : dayAndTime.length());
        String eventTime = dayAndTime.substring(posTimeStart, posTimeEnd);

        Date eventDate = DATE_FORMAT_DAY_TIME.parse(eventDay + thisYear + " " + eventTime);

        Elements descriptions = forElement.select(".event-description p:not(.event-day-and-time)");
        String eventDescription = "";
        for (Element forDescription : descriptions) {
            eventDescription += forDescription.text() + "\n\n";
        }

        Category eventCategory = Category.MISC;
        Elements capitals = forElement.select("span.caps");
        for (Element forCapital : capitals) {
            String forCapitalText = forCapital.text();

            for (Map.Entry<String, Category> forMapEntry : CATEGORY_BY_LABEL.entrySet()) {
                if (forCapitalText.contains(forMapEntry.getKey())) {
                    eventCategory = forMapEntry.getValue();
                    break;
                }
            }
        }

        Log.v(LOG_NET, "currentEvent: " + currentEvent);
        Log.v(LOG_NET, "eventDay: " + eventDay);
        Log.v(LOG_NET, "title: " + title);
        Log.v(LOG_NET, "imageURL: " + imageURL);
        Log.v(LOG_NET, "eventTime: " + eventTime);
        Log.v(LOG_NET, "eventDescription: " + eventDescription);
        Log.v(LOG_NET, "eventCategory: " + eventCategory);

        Event newEvent = new Event(title,
        null,
        eventDescription,
        eventDate,
        LOCATION_NAME,
        new LinkedList<String>(),
        imageURL,
        eventCategory,
        PROVIDER_NAME,
        providerId,
        null,
        downloader.downloaderId);

        newEvent.locationURL = BASE_URL;
        newEvent.eventURL = CALENDAR_URL;

        result.add(newEvent);

        currentEvent++;
    }

    @Override
    public int getProgress() {
        return (int) (100.0 * currentEvent / numEvents);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        return false;
    }
}
