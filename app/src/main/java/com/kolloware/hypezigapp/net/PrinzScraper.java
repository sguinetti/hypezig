package com.kolloware.hypezigapp.net;

import android.net.TrafficStats;
import android.util.Log;

import com.kolloware.hypezigapp.models.Category;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Event;
import com.kolloware.hypezigapp.util.JSoupUtil;
import com.kolloware.hypezigapp.util.NestedIterator;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;
import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class PrinzScraper implements Scraper {
    private static final int THREAD_ID = PrinzScraper.class.getName().hashCode();

    private static final String PROVIDER_NAME = "prinz.de";

    private static final SimpleDateFormat URL_DATE_FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd");

    private static final SimpleDateFormat EVENT_DATETIME_FORMAT = new SimpleDateFormat(
            "dd. MMMMMMMM yyyy, HH:mm", Locale.GERMANY);
    private static final String ELEMENT_EVENTS = "article.event";
    private static final int NUM_DAYS_TO_SCRAPE = 30;


    private Iterator<Element> elementIterator;
    private int numEvents = 1;
    private int eventCounter = 0;

    private List<Event> localResult = new ArrayList<>();
    private DownloadStatus localDownloadStatus = DownloadStatus.RUNNING;
    private Downloader downloader;


    public PrinzScraper(Downloader downloader) throws Exception {
       this.downloader = downloader;
    }

    private Elements getEventsByPath(String inPath) throws Exception {
        String url = "https://prinz.de/leipzig/events/" + inPath;

        Log.i(LOG_NET, "fetchEvents: URL to be read = " + url);

        Connection connection = Jsoup.connect(url);
        connection.maxBodySize(0);
        Connection.Response response = connection.timeout(2 * 60 * 1000).execute();

        Document doc = Jsoup.parse(response.body());

        return doc.select(ELEMENT_EVENTS);
    }

    public void init() throws Exception {

        TrafficStats.setThreadStatsTag(THREAD_ID);

        Calendar cal = Calendar.getInstance();

        List<Iterator<Element>> allElements = new ArrayList<>();

        // Get current week
        Elements elementsCurrentWeek = getEventsByPath("7-tage");
        numEvents = elementsCurrentWeek.size();
        allElements.add(elementsCurrentWeek.iterator());
        Log.d(LOG_NET, "numEvents current week: " + numEvents);

        // Get rest of month
        cal.add(Calendar.DAY_OF_MONTH, 7);
        for (int i = 7; i <= NUM_DAYS_TO_SCRAPE; i++) {
            String parsedDate = URL_DATE_FORMAT.format(cal.getTime());

            Elements newElements = getEventsByPath(parsedDate);
            numEvents += newElements.size();
            Log.d(LOG_NET, "numEvents " + parsedDate + ": " + newElements.size());
            allElements.add(newElements.iterator());

            cal.add(Calendar.DAY_OF_MONTH, 1);
        }

        Log.d(LOG_NET, "numEvents total: " + numEvents);

        elementIterator = new NestedIterator<Element>(allElements);
    }

    private final static Map<String, Category> CATEGORY_MAP = new HashMap<String, Category>() {{
        put("Familie & Kinder Events", Category.FAMILY);
        put("Sportevents", Category.SPORTS);
        put("Party", Category.PARTY);
        put("Konzerte & Live-Musik", Category.MUSIC);
        put("Brunch Events", Category.FOOD);
        put("Diskussionsrunden & Vorträge", Category.LECTURES);
        put("Führungen", Category.GUIDED_TOUR);
        put("Gastro Events", Category.FOOD);
        put("Shopping Events", Category.SHOPPING);
        put("Ausstellungen", Category.ART);
        put("Film & Kino Events", Category.CINEMA);
        put("Kunst Events", Category.ART);
        put("Lesungen", Category.LECTURES);
        put("Theater & Bühnen Events", Category.STAGE);
        put("Web, TV & Radio Events", Category.CULTURE);
    }};

    @Override
    public DownloadStatus getResult() {
        return localDownloadStatus;
    }

    @Override
    public List<Event> getEvents() {
        return localResult;
    }

    @Override
    public boolean hasNext() {
        if (elementIterator.hasNext()) {
            return true;
        }
        else {
            if (localDownloadStatus == DownloadStatus.RUNNING) {
                localDownloadStatus = DownloadStatus.SUCCESS;
            }
            return false;
        }
    }

    /**
     * Extract tags from an event
     *
     * There are currently no tags visible, they are commented out.
     * We therefor need to read the comment and uncomment the tag.
     *
     * @param event - an event element
     * @return list of tags
     */
    private Set<String> getTags(Element event) {
        Log.d(LOG_NET, getClass().getSimpleName() + ".getTags() called");

        Set<String> result = new HashSet<>();

        try {
            List<Node> childNodes = event.childNodes();
            for (Node forChild : childNodes) {
                if (forChild instanceof Comment) {
                    Comment comment = (Comment) forChild;
                    String content = comment.getData();

                    Document d = Jsoup.parse(content);

                    String tagBadge = (d.selectFirst(".prinz-event-teaser-badge")).text();
                    String[] tags = tagBadge.split(",");

                    for (String forTag : tags) {
                        result.add(forTag.trim());
                    }
                }
            }
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.e(LOG_NET, Log.getStackTraceString(e));
        }

        return result;
    }

    @Override
    public void scrapeNext() throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".scrapeNext() called");

        Element event = elementIterator.next();
        Log.d(LOG_NET, "scraping: " + event.text());

        String title = event.select(".prinz-event-teaser-title").text();

        String locationName = event.select(".prinz-event-teaser-meta-location").text();

        Set<String> tagSet = getTags(event);

        Category bestCategory = null;

        for (String forTag : tagSet) {
            Category category = CATEGORY_MAP.get(forTag);
            if (category != null) {
                bestCategory = category;
            }
        }

        String imageCSS = event.select("figure").attr("style");
        String imageURL = imageCSS.replace("background-image: url('", "")
                .replace("')", "");

        Category category = (bestCategory != null) ? bestCategory : Category.MISC;

        String id = event.attr("id")
                .replace("prinz-event-teaser-", "");

        String eventURL = event.select(".prinz-stretched-link").attr("href");

        String eventTimeText = event.select(".prinz-event-teaser-meta-date").text();

        String eventDateTime = eventTimeText.substring(
                eventTimeText.indexOf(",") + 2,
                eventTimeText.lastIndexOf(" Uhr"));

        Date eventDate = EVENT_DATETIME_FORMAT.parse(eventDateTime);

        String prinzId = eventDateTime + "-" + id;

        Event newEvent = new Event(title,
                null,
                null,
                eventDate,
                locationName,
                new ArrayList<>(tagSet),
                (imageURL.length() > 0) ? imageURL : null,
                category,
                PROVIDER_NAME,
                prinzId,
                tagSet.toString(),
                downloader.downloaderId);

        newEvent.eventURL = eventURL;

        Log.d(LOG_APP, "New event: " + newEvent);

        localResult.add(newEvent);

        eventCounter++;
    }

    @Override
    public int getProgress() {
        return  (int) (100.0 * eventCounter / numEvents);
    }

    @Override
    public boolean updateEvent(Event inOutEvent) throws Exception {
        Log.d(LOG_NET, getClass().getSimpleName() + ".scrapeAdditionalInformation() called "
                + "with: inOutEvent = [" + inOutEvent + "]");

        if (inOutEvent.details != null && !inOutEvent.details.isEmpty()) {
            // No update needed
            return false;
        }
        else {
            TrafficStats.setThreadStatsTag(THREAD_ID);

            Document doc = JSoupUtil.getDocumentFromURL(inOutEvent.eventURL);

            String details = doc.select(".prinz-text").text();
            String locationURL = doc.select("address div[itemprop='name'] a").attr("href");

            inOutEvent.details = details;
            inOutEvent.locationURL = locationURL;

            return true;
        }
    }
}

