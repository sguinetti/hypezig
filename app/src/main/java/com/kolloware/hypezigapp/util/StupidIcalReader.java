package com.kolloware.hypezigapp.util;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class StupidIcalReader {

    private static String removeEscapeCharacters(String inString) {
        return inString.replace("\\n", "\n")
                .replace("\\", "")
                .replace("&amp;", "&")
                .replace("&nbsp;", "\n");
    }

    public static JSONObject getIcalAsJSON(String inURL) throws Exception {
        Log.d(LOG_NET, StupidIcalReader.class.getSimpleName()
                + ".getIcalAsJSON() called with: inURL = [" + inURL + "]");

        JSONObject localResult = new JSONObject();

        try {
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(new URL(inURL).openStream()));

            String forLine;

            List<String> contents = new LinkedList<>();

            while ((forLine = input.readLine()) != null)  {
                if (forLine.startsWith(" ")) {
                    int indexLast = contents.size() - 1;
                    String lastLine = contents.get(indexLast);
                    lastLine += forLine.substring(1);
                    contents.remove(indexLast);
                    contents.add(lastLine);
                }
                else {
                    contents.add(forLine);
                }
            }

            Stack<JSONObject> stack = new Stack<>();
            stack.push(localResult);
            JSONObject currentObject = localResult;

            for (String forString : contents) {
                Log.v(LOG_NET, "content: " + forString);

                int posColon = forString.indexOf(":");
                String key = forString.substring(0, posColon);
                String value = forString.substring(posColon + 1);

                if (key.equals("BEGIN")) {
                    if (!currentObject.has(value)) {
                        currentObject.put(value, new JSONArray());
                    }

                    JSONArray array = (JSONArray) currentObject.get(value);
                    JSONObject newObject = new JSONObject();
                    array.put(newObject);

                    stack.push(currentObject);
                    currentObject = newObject;
                }
                else if (key.equals("END")) {
                    currentObject = stack.pop();
                }
                else {
                    currentObject.put(removeEscapeCharacters(key),
                            removeEscapeCharacters(value));
                }
            }

            return localResult;
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.e(LOG_NET, Log.getStackTraceString(e));
            throw (e);
        }

    }

}
