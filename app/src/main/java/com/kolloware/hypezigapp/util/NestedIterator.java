package com.kolloware.hypezigapp.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NestedIterator<T> implements Iterator {

    private Iterator<Iterator<T>> iterators;
    private Iterator<T> currentIterator = (new ArrayList<T>()).iterator();

    public NestedIterator(List<Iterator<T>> inIterators) {
        iterators = inIterators.iterator();
    }

    @Override
    public boolean hasNext() {
        if (currentIterator.hasNext()) return true;

        if (iterators.hasNext()) {
            currentIterator = iterators.next();
            return currentIterator.hasNext();
        }
        else {
            return false;
        }
    }

    @Override
    public T next() {
        T result = currentIterator.next();

        return result;
    }
}
