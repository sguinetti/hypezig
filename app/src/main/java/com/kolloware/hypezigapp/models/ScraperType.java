package com.kolloware.hypezigapp.models;

import com.kolloware.hypezigapp.net.ConfigurableFacebookScraper;
import com.kolloware.hypezigapp.net.ConfigurableICalScraper;
import com.kolloware.hypezigapp.net.FrauenkulturScraper;
import com.kolloware.hypezigapp.net.KreuzerScraper;
import com.kolloware.hypezigapp.net.PrinzScraper;
import com.kolloware.hypezigapp.net.Sachsenpunk;
import com.kolloware.hypezigapp.net.Scraper;
import com.kolloware.hypezigapp.net.SoundOfLeipzigScraper;
import com.kolloware.hypezigapp.net.TrichterScraper;

public enum ScraperType {
    KREUZER,
    PRINZ,
    SACHSENPUNK,
    FACEBOOK,
    FRAUENKULTUR,
    TRICHTER,
    ICAL,
    FACEBOOK_PAGE;

    public static Class<? extends Scraper> getScraperClassByType(ScraperType inType) {
        switch (inType) {
            case KREUZER:
                return KreuzerScraper.class;
            case PRINZ:
                return PrinzScraper.class;
            case SACHSENPUNK:
                return Sachsenpunk.class;
            case FACEBOOK:
                return SoundOfLeipzigScraper.class;
            case FRAUENKULTUR:
                return FrauenkulturScraper.class;
            case TRICHTER:
                return TrichterScraper.class;
            case ICAL:
                return ConfigurableICalScraper.class;
            case FACEBOOK_PAGE:
                return ConfigurableFacebookScraper.class;
        }

        return null;
    }
}

