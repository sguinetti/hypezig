package com.kolloware.hypezigapp.models.filters;

import java.util.Calendar;
import java.util.Date;

public class WeekendPreset implements DatePreset {

    private Date friday, sunday;

    public WeekendPreset() {
        Calendar calendar = Calendar.getInstance();

        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        sunday = calendar.getTime();

        calendar.add(Calendar.DAY_OF_MONTH, -2);

        friday = calendar.getTime();
    }

    @Override
    public Date getDateFrom() {
        return friday;
    }

    @Override
    public Date getDateTo() {
        return sunday;
    }
}
