package com.kolloware.hypezigapp.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.kolloware.hypezigapp.db.DateConverter;
import com.kolloware.hypezigapp.db.DownloadResultConverter;
import com.kolloware.hypezigapp.db.PropertiesConverter;
import com.kolloware.hypezigapp.db.ScraperTypeConverter;
import com.kolloware.hypezigapp.tasks.EventScraperTask;

import org.json.JSONObject;

import java.util.Date;

@Entity
@TypeConverters({DateConverter.class,
        DownloadResultConverter.class,
        ScraperTypeConverter.class,
        PropertiesConverter.class})
public class Downloader {

    @PrimaryKey(autoGenerate = true) public int downloaderId;

    private String title;
    private int progress;
    private boolean running = false;
    private Date lastUpdate;
    private DownloadStatus result;
    private JSONObject properties;

    private ScraperType scraperType;

    @Ignore
    private EventScraperTask task;

    @Ignore
    public final static String PROPERTY_URL = "URL";

    public Downloader(String title) {
        this.title = title;
    }

    public String getTitle() { return title; }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getProgress() { return progress; }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public boolean isRunning() { return running; }

    public void setRunning(boolean running) { this.running = running; }

    public void cancelTask() {
        if (task != null) {
            task.cancel();
        }
    }

    public Date getLastUpdate() { return lastUpdate; }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public DownloadStatus getResult() { return result; }

    public void setResult(DownloadStatus result) {
        this.result = result;
    }

    public void setTask(EventScraperTask task) { this.task = task; }

    public ScraperType getScraperType() {
        return scraperType;
    }

    public void setScraperType(ScraperType scraperType) {
        this.scraperType = scraperType;
    }

    public JSONObject getProperties() { return properties; }

    public void setProperties(JSONObject properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        String localResult = "Downloader: ";

        localResult += "id: " + downloaderId + ", ";
        localResult += "type: " + scraperType + ", ";
        localResult += "progress: " + progress + ", ";
        localResult += "result: " + result + ", ";
        localResult += "running: " + running + ", ";
        localResult += "properties: " + properties + ", ";
        localResult += "last update: " + lastUpdate;

        return localResult;
    }
}
