package com.kolloware.hypezigapp.models;

import android.graphics.Bitmap;
import android.util.Log;

import com.kolloware.hypezigapp.R;
import com.kolloware.hypezigapp.models.filters.CategoryFilter;
import com.kolloware.hypezigapp.models.filters.DateFilter;
import com.kolloware.hypezigapp.models.filters.DatePreset;
import com.kolloware.hypezigapp.db.queries.QueryStrategy;
import com.kolloware.hypezigapp.db.queries.SortByDate;
import com.kolloware.hypezigapp.models.filters.TodayPreset;
import com.kolloware.hypezigapp.ui.DownloadsRecyclerViewAdapter;
import com.kolloware.hypezigapp.ui.UITools;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.kolloware.hypezigapp.BaseApplication.LOG_APP;

public class Model {

    private static Model instance = null;

    private ArrayList<Event> orderedEvents = new ArrayList<>();
    private ArrayList<Event> filteredEvents = new ArrayList<>();
    private ArrayList<Event> favorites = new ArrayList<>();

    private QueryStrategy queryStrategy = new SortByDate();
    private DateFilter dateFilter;
    private CategoryFilter categoryFilter = new CategoryFilter();

    private Map<String, Bitmap> imageCache = new HashMap<>();


    // View model variables
    private List<Downloader> downloadersToShow = new ArrayList<>();
    private Date dateFrom = new Date();
    private Date dateTo = new Date();
    private boolean multiDownloadRunning = false;
    private int mainActivityNavigationFragmentId = R.id.nav_home;
    private DownloadsRecyclerViewAdapter downloadsRecyclerViewAdapter = null;

    private Model() {
        Log.d(LOG_APP, getClass().getSimpleName() + " constructed");

        DatePreset todayPreset = new TodayPreset();
        dateFilter = new DateFilter(todayPreset.getDateFrom(), todayPreset.getDateTo());
    }

    public static Model getInstance() {
        Log.d(LOG_APP, Model.class.getName() + ".getInstance() called");

        if (instance == null) instance = new Model();
        return instance;
    }

    public void applyFilter() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".applyFilter() called");

        List<Event> localResult = new ArrayList<>();
        dateFilter.applyFilter(orderedEvents, localResult);
        categoryFilter.applyFilter(localResult, filteredEvents);
    }

    public List<Event> getOrderedEvents() {
        return orderedEvents;
    }

    public List<Event> getFilteredEvents() {
        return filteredEvents;
    }

    public QueryStrategy getQueryStrategy() {
        return queryStrategy;
    }

    public List<Event> getFavorites() { return favorites; }

    public CategoryFilter getCategoryFilter() {
        return categoryFilter;
    }

    public void setQueryStrategy(QueryStrategy queryStrategy) {
        Log.d(LOG_APP, getClass().getSimpleName() + ".setQueryStrategy() called");

        this.queryStrategy = queryStrategy;
    }

    public void updateDateFilter() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".updateDateFilter() called");

        this.dateFilter = new DateFilter(dateFrom, dateTo);
        applyFilter();
    }

    public Map<String, Bitmap> getImageCache() { return imageCache; }

    public List<Downloader> getDownloadersToShow() { return downloadersToShow; }


    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isMultiDownloadRunning() {
        return multiDownloadRunning;
    }

    public void setMultiDownloadRunning(boolean multiDownloadRunning) {
        this.multiDownloadRunning = multiDownloadRunning;
    }

    public int getMainActivityNavigationFragmentId() {
        return mainActivityNavigationFragmentId;
    }

    public void setMainActivityNavigationFragmentId(int mainActivityNavigationFragmentId) {
        this.mainActivityNavigationFragmentId = mainActivityNavigationFragmentId;
    }

    public DownloadsRecyclerViewAdapter getDownloadsRecyclerViewAdapter() {
        return downloadsRecyclerViewAdapter;
    }

    public void setDownloadsRecyclerViewAdapter(DownloadsRecyclerViewAdapter downloadsRecyclerViewAdapter) {
        this.downloadsRecyclerViewAdapter = downloadsRecyclerViewAdapter;
    }
}
