package com.kolloware.hypezigapp.models;

import android.content.Context;

import com.kolloware.hypezigapp.R;

import java.util.ArrayList;
import java.util.List;

public class ScraperTypeInformation {

    public ScraperType scraperType;
    public String title, description, url;

    public ScraperTypeInformation(ScraperType scraperType,
                                  String title, String description, String url) {
        this.scraperType = scraperType;
        this.title = title;
        this.description = description;
        this.url = url;
    }

    public static List<ScraperTypeInformation> getDirectory(final Context inContext) {
        List<ScraperTypeInformation> items = new ArrayList<ScraperTypeInformation>();

        items.add(new ScraperTypeInformation(ScraperType.FRAUENKULTUR,
                inContext.getString(R.string.scraper_frauenkultur_title),
                inContext.getString(R.string.scraper_frauenkultur_description),
                "https://www.frauenkultur-leipzig.de/"));
        items.add(new ScraperTypeInformation(ScraperType.KREUZER,
                    inContext.getString(R.string.scraper_kreuzer_title),
                    inContext.getString(R.string.scraper_kreuzer_description),
                    inContext.getString(R.string.scraper_kreuzer_url)));
        items.add(new ScraperTypeInformation(ScraperType.PRINZ,
                inContext.getString(R.string.scraper_prinz_title),
                inContext.getString(R.string.scraper_prinz_description),
                inContext.getString(R.string.scraper_prinz_url)));
        items.add(new ScraperTypeInformation(ScraperType.SACHSENPUNK,
                    inContext.getString(R.string.scraper_sachsenpunk_title),
                    inContext.getString(R.string.scraper_sachsenpunkt_description),
                    inContext.getString(R.string.scraper_sachsenpunk_url)));
        items.add(new ScraperTypeInformation(ScraperType.FACEBOOK,
                    inContext.getString(R.string.scraper_sol_title),
                    inContext.getString(R.string.scraper_sol_description),
                    inContext.getString(R.string.scraper_sol_url)));
        items.add(new ScraperTypeInformation(ScraperType.TRICHTER,
                inContext.getString(R.string.scraper_trichter_title),
                inContext.getString(R.string.scraper_trichter_description),
                "https://trichter.cc"));
        items.add(new ScraperTypeInformation(ScraperType.ICAL,
                "iCal",
                inContext.getString(R.string.scraper_ical_description),
                null));
        items.add(new ScraperTypeInformation(ScraperType.FACEBOOK_PAGE,
                inContext.getString(R.string.scraper_facebook_title),
                inContext.getString(R.string.scraper_facebook_description),
                null));

        return items;
    }
}


