package com.kolloware.hypezigapp.models.filters;

import java.util.Calendar;
import java.util.Date;

public class TodayPreset implements DatePreset {

    private Date today;

    public TodayPreset() {
        today = Calendar.getInstance().getTime();
    }

    @Override
    public Date getDateFrom() {
        return today;
    }

    @Override
    public Date getDateTo() {
        return today;
    }
}
