package com.kolloware.hypezigapp.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.models.Downloader;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;

public class DeleteDownloaderTask extends AsyncTask<Void, Void, Void> {

    private Downloader downloader;
    private Context context;

    public DeleteDownloaderTask(Downloader downloader, Context context) {
        this.downloader = downloader;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d(LOG_DATA, getClass().getSimpleName() + ".doInBackground() called");

        AppDatabase database = AppDatabase.getInstance(context);

        // delete downloader
        database.downloaderDao().delete(downloader.downloaderId);

        // delete all associated events
        database.eventDao().deleteByDownloaderId(downloader.downloaderId);

        return null;
    }
}
