package com.kolloware.hypezigapp.tasks;

import android.content.Context;
import android.util.Log;

import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.db.DatabaseUtils;
import com.kolloware.hypezigapp.models.DownloadStatus;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.ScraperType;
import com.kolloware.hypezigapp.net.Scraper;
import com.kolloware.hypezigapp.ui.UICallback;

import java.lang.reflect.Constructor;
import java.util.Date;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;
import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;
import static com.kolloware.hypezigapp.BaseApplication.LOG_UI;
import static com.kolloware.hypezigapp.models.DownloadStatus.CANCELLED;

public class EventScraperTask extends AbstractChainableTask {

    private Downloader downloader;
    private Scraper scraper;
    private UICallback uiCallback;
    private Context context;

    private boolean cancelled = false;
    private boolean runDeduplication = true;

    public EventScraperTask(Downloader downloader,
                            UICallback uiCallback,
                            Context context,
                            ChainableTaskCallback taskCallback,
                            boolean runDeduplication) {
        super(taskCallback);

        this.downloader = downloader;
        this.uiCallback = uiCallback;
        this.context = context;
        this.runDeduplication = runDeduplication;

        downloader.setTask(this);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d(LOG_NET, getClass().getSimpleName() + ".doInBackground() called");

        try {
            // Initialize
            downloader.setResult(DownloadStatus.INITIALIZING);
            downloader.setRunning(true);
            uiCallback.refresh();

            Class<? extends Scraper> clazz = ScraperType.getScraperClassByType(downloader.getScraperType());
            Constructor constructor = clazz.getConstructor(Downloader.class);
            scraper = (Scraper) constructor.newInstance(downloader);
            scraper.init();

            // Start scraping
            downloader.setResult(DownloadStatus.RUNNING);
            uiCallback.refresh();

            while (!cancelled && scraper.hasNext()) {
                scraper.scrapeNext();

                downloader.setProgress(scraper.getProgress());
                uiCallback.refresh();
            }

            if (!cancelled) {
                // Write to database
                scraper.getEvents();

                downloader.setResult(DownloadStatus.FINALIZING);
                uiCallback.refresh();

                DatabaseUtils.insertOrUpdateAllEvents(scraper.getEvents(),
                        context);

                downloader.setResult(scraper.getResult());

                if (runDeduplication) {
                    DatabaseUtils.hideDuplicates(context);
                }
            }
            else {
                downloader.setResult(CANCELLED);
            }
        }
        catch (Exception e) {
            Log.e(LOG_UI, "Error while running scraper task: " + e.getMessage());
            Log.i(LOG_UI, "Stack trace: " + Log.getStackTraceString(e));
            downloader.setResult(DownloadStatus.ERROR);
        }

        downloader.setLastUpdate(new Date());
        downloader.setRunning(false);

        AppDatabase.getInstance(context).downloaderDao().update(downloader);

        Log.d(LOG_NET, "doInBackground: Task finished");

        return null;
    }

    @Override
    protected void onTaskFinished(Void aVoid) {
        Log.d(LOG_DATA, getClass().getSimpleName() + ".onPostExecute() called");
        Log.v(LOG_DATA, "lastUpdate = " + downloader.getLastUpdate().toString());
        Log.v(LOG_DATA, "progress = " + downloader.getProgress());
        Log.v(LOG_DATA, "result = " + downloader.getResult().toString()) ;
        uiCallback.refresh();
    }

    public void cancel() {
        Log.d(LOG_DATA, getClass().getSimpleName() + ".cancel() called");
        cancelled = true;
    }
}
