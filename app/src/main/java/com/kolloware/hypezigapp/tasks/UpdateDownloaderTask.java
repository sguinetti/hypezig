package com.kolloware.hypezigapp.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.models.Downloader;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;

public class UpdateDownloaderTask extends AsyncTask<Void, Void, Void> {

    private Downloader downloader;
    private Context context;

    public UpdateDownloaderTask(Downloader downloader, Context context) {
        this.downloader = downloader;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d(LOG_DATA, getClass().getSimpleName() + ".doInBackground() called");

        AppDatabase database = AppDatabase.getInstance(context);

        // insert downloader
        database.downloaderDao().update(downloader);

        return null;
    }
}
