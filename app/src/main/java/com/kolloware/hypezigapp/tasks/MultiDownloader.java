package com.kolloware.hypezigapp.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kolloware.hypezigapp.db.DatabaseUtils;
import com.kolloware.hypezigapp.models.Downloader;
import com.kolloware.hypezigapp.models.Model;
import com.kolloware.hypezigapp.ui.UICallback;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_NET;

public class MultiDownloader implements ChainableTaskCallback {

    Iterator<AbstractChainableTask> taskIterator;

    public MultiDownloader(List<Downloader> downloaders,
                           UICallback uiCallback,
                           final Context context) {
        Log.d(LOG_NET, getClass().getSimpleName() + ".MultiDownloader() called with: " +
                "downloaders = [" + downloaders + "], uiCallback = [" + uiCallback +
                "], context = [" + context + "]");

        List<AbstractChainableTask> taskList = new LinkedList<>();

        for (Downloader forDownloader : downloaders) {
            AbstractChainableTask forTask = new EventScraperTask(forDownloader,
                    uiCallback,
                    context,
                    this,
                    false);

            taskList.add(forTask);
        }

        taskList.add(new AbstractChainableTask(this) {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseUtils.hideDuplicates(context);
                return null;
            }
        });

        taskIterator = taskList.iterator();
    }

    public void start() {
        Log.d(LOG_NET, getClass().getSimpleName() + ".start() called");

        Model.getInstance().setMultiDownloadRunning(true);
        this.notifyPostExecute();
    }

    @Override
    public void notifyPostExecute() {
        Log.d(LOG_NET, getClass().getSimpleName() + ".notifyPostExecute() called");

        if (taskIterator.hasNext()) {
            taskIterator.next().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else {
            Model.getInstance().setMultiDownloadRunning(false);
        }
    }
}
