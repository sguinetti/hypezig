package com.kolloware.hypezigapp.tasks;

public class VoidCallback implements ChainableTaskCallback {

    @Override
    public void notifyPostExecute() {
        // do nothing
    }
}
