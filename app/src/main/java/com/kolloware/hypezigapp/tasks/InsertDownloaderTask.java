package com.kolloware.hypezigapp.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kolloware.hypezigapp.db.AppDatabase;
import com.kolloware.hypezigapp.models.Downloader;

import java.util.List;

import static com.kolloware.hypezigapp.BaseApplication.LOG_DATA;

public class InsertDownloaderTask extends AsyncTask<Void, Void, Void> {

    private Downloader downloader;
    private Context context;

    public InsertDownloaderTask(Downloader downloader, Context context) {
        this.downloader = downloader;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        Log.d(LOG_DATA, getClass().getSimpleName() + ".doInBackground() called");

        AppDatabase database = AppDatabase.getInstance(context);

        // insert downloader
        List<Long> result = database.downloaderDao().insertAll(downloader);

        downloader.downloaderId = result.get(0).intValue();

        return null;
    }
}
