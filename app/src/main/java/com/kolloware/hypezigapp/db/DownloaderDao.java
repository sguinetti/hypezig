package com.kolloware.hypezigapp.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.kolloware.hypezigapp.models.Downloader;

import java.util.List;

@Dao
public interface DownloaderDao {

    @Query("SELECT * from downloader WHERE downloaderId = :downloaderId")
    Downloader getByDownloaderId(int downloaderId);

    @Query("SELECT * FROM downloader ORDER BY title")
    List<Downloader> getAll();

    @Update
    void update(Downloader downloader);

    @Query("DELETE FROM downloader")
    void deleteAll();

    @Query("DELETE FROM downloader WHERE downloaderId = :downloaderId")
    void delete(int downloaderId);

    @Insert
    List<Long> insertAll(Downloader... downloaders);
}
