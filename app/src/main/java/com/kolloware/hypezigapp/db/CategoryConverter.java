package com.kolloware.hypezigapp.db;

import androidx.room.TypeConverter;

import com.kolloware.hypezigapp.models.Category;

public class CategoryConverter {

    @TypeConverter
    public static Category toCategory(String categoryString){
        if (categoryString == null) return null;
        return Category.valueOf(categoryString);
    }

    @TypeConverter
    public static String fromCategory(Category category){
        if (category == null) return null;
        return category.toString();
    }
}
